#!/usr/bin/ruby

require "rubygems"
require "gnuplot"

#variables for genetic algorithm population
$popsize = 2000
$xfactor = 10
$yfactor = 20
$elitekept = 0
$roulettekept = 15
$iterations = 250
$games = 50
$method = "elitism"
$mutrate = 1000
$randomers = 0

#fitness function for iterated prisoners dilemma
def dilemma(genepool, gene)

    #rival genes are the entire genepool including the current gene
    others = genepool

    #gene has to compete with every rival
    others.each do |other|

        #both gene's history/percieved history of opponent is reset to value from bitstring
        gene.resethistorys
        other.resethistorys

        #iterations of prisoners dilemma with cumulative scoring
        $games.times do

            #each gene's choice is influenced by own history and percieved history of opponent
            genechoice = gene.history + gene.ohistory
            otherchoice = other.history + other.ohistory
            
            #game played is recorded
            gene.game
            other.game

            #truth chosen by first gene is remembered by both genes
            if gene.bitstr[Integer("0b"+genechoice)] == "1"
                gene.updatehistory("self","1")
                other.updatehistory("other","1")
                gchoice = "truth"
            
            #lie chosen by first gene is remembered by both genes
            else                
                gene.updatehistory("self","0")
                other.updatehistory("other","0")
                gchoice = "lie"
            end

            #truth chosen by second gene is remembered by both genes
            if other.bitstr[Integer("0b"+genechoice)] == "1"
                other.updatehistory("self","1")
                gene.updatehistory("other","1")
                ochoice = "truth"

            #lie chosen by second gene is remembered by both genes
            else
                ochoice = "lie"
                other.updatehistory("self","0")
                gene.updatehistory("other","0")
            end

            #in case of a compromise, both genes gain 3 fitness
            if (gchoice == "truth" && ochoice == "truth")
                gene.incfitness(3)
                other.incfitness(3)

            #when both genes lie, both gain 1 fitness
            elsif (gchoice == "lie" && ochoice == "lie")
                gene.incfitness(1)
                other.incfitness(1)

            #when one gene betrays the other, the betrayer gains 5 fitness and the victim gets nothing
            elsif (gchoice == "lie" && ochoice == "truth")
                gene.incfitness(5)
                other.incfitness(0)
            elsif (gchoice == "truth" && ochoice == "lie")
                gene.incfitness(0)
                other.incfitness(5)
            end

        end
    end
end

#shows average fitness of top 5 genes
def topfive(pop)
    top = pop[0..4]
    total = 0
    top.each do |gene|
        total+= gene.fitness
    end
    average = Integer(total/top.size)
    puts "Average fitness of top 5 = #{average}"
    average
end

#shows the gene with the highest fitness
def showleader(pop,fithis)
    leader = pop[0]
    puts "Leader fitness = #{leader.fitness}"
    puts leader.bitstr
    fithis << leader.fitness
end

#show average fitness of all genes
def avall(pop,fithis)
    total = 0
    pop.each do |gene|
        total+= gene.fitness
    end
    average = Float(total/pop.size)
    puts "Average fitness = #{average}"
    average
    fithis << average
end

#shows variance across all genes
def variance(pop,mean,varhist)
    total = 0
    pop.each do |gene|
        total+= (gene.fitness-mean[mean.size-1])**2
    end
    total = Float(total/pop.size)
    puts "Variance is #{total}"
    varhist << total
end

#Genotype class definition loaded
require_relative "genotype"

#begin genetic algorithm this file is loaded in interpreter
if __FILE__ == $0

    #initial population of random genes
    pop = []
    $popsize.times do
        pop << Genotype.new("random")
    end

    #population is added to the torus   
    torus = Hash.new
    
    $xfactor.times do |x|
        $yfactor.times do |y|
            torus[[x,y]] = pop[0]
            pop = pop - [pop[0]]
        end
    end


    #mutation rate set
    torus[[0,0]].setrate($mutrate)

    #empty arrays of fitness history
    leaderhistory = []
    averagehistory = []
    variancehistory = []
    populationsize = []

    #reset the fitness and games played by all genes on the torus
    $xfactor.times do |x|
        $yfactor.times do |y|
            torus[[x,y]].resetgames
            torus[[x,y]].resetfitness
        end
    end

    #test grid member against neighbours by position
    $xfactor.times do |x|
        $yfactor.times do |y|
            center = torus[[x,y]]
            edge = []
            [-1,0,1].each do |h|
                [-1,0,1].each do |v|
                    edge << torus[[(x+h) % $xfactor , (y+v) % $yfactor]]
                end
            end
            dilemma(edge,center)
        end
    end

    #normalise the fitness of each gene
    torus[[0,0]].totalfitness = 0
    $xfactor.times do |x|
        $yfactor.times do |y|
            puts torus[[x,y]].games
            normalfit = (torus[[x,y]].fitness / (torus[[x,y]].games * 5))
            torus[[x,y]].resetfitness
            torus[[x,y]].incfitness(Float(normalfit))
            torus[[x,y]].totalfitness=(torus[[x,y]].totalfitness + normalfit)
            puts torus[[x,y]].fitness
        end
    end

    #extract population from torus to display information
    pop = []
    $xfactor.times do |x|
        $yfactor.times do |y|
            pop << torus[[x,y]]
        end
    end

    #display information about population
    pop.sort!{ |b,a| a.fitness <=> b.fitness }
    topfive(pop)
    avall(pop,averagehistory)
    showleader(pop,leaderhistory)
    variance(pop,averagehistory,variancehistory)

    #iterations of genetic algorithm
    $iterations.times do |time|
        
        #output current generation
        puts "Generation #{time+1}"

        
        #reset the fitness and games played by all genes on the torus
        $xfactor.times do |x|
            $yfactor.times do |y|
                torus[[x,y]].resetgames
                torus[[x,y]].resetfitness
            end
        end

        #test grid member against neighbours by position
        $xfactor.times do |x|
            $yfactor.times do |y|
                center = torus[[x,y]]
                edge = []
                 [-1,0,1].each do |h|
                    [-1,0,1].each do |v|
                        edge << torus[[(x+h) % $xfactor , (y+v) % $yfactor]]
                    end
                end
                dilemma(edge,center)
            end
        end

        #normalise the fitness of each gene
        torus[[0,0]].totalfitness = 0
        $xfactor.times do |x|
            $yfactor.times do |y|
                normalfit = (torus[[x,y]].fitness / (torus[[x,y]].games * 5))
                torus[[x,y]].resetfitness
                torus[[x,y]].incfitness(Float(normalfit))
                torus[[x,y]].totalfitness=(torus[[x,y]].totalfitness + normalfit)
                puts torus[[x,y]].fitness
            end
        end

        
       #extract population from torus to display information
        pop = []
        $xfactor.times do |x|
            $yfactor.times do |y|
                pop << torus[[x,y]]
            end
        end

        #display information about population
        pop.sort!{ |b,a| a.fitness <=> b.fitness }
        puts "before error"
        puts pop
        topfive(pop)
        puts "after error"
        avall(pop,averagehistory)
        showleader(pop,leaderhistory)
        variance(pop,averagehistory,variancehistory)
 
        
        
        #select and crossover regions of the torus
        $xfactor.times do |x|
            $yfactor.times do |y|

                #sort each reigion into descending fitness
                region = []
                [-1,0,1].each do |h|
                    [-1,0,1].each do |v|
                        region << torus[[(x+h) % $xfactor , (y+v) % $yfactor]]
                    end
                end
                region.sort!{ |b,a| a.fitness <=> b.fitness } 

                #store  the 5 out of 9 with the highest fitness
                saved = []
                5.times do
                    saved << region[0]
                    region = region - [region[0]]
                end

                #use the elites to produce 4 more children
                babies = []
                while babies.size < 4
                    baby = Genotype.new(cross(saved[rand(5)],saved[rand(5)]))
                    baby.resetfitness
                    #baby.incfitness(100000)
                    babies << baby
                end

                #replace any non elites with a newly generated child
                [-1,0,1].each do |h|
                    [-1,0,1].each do |v|
                        if not(saved.include?(torus[[(x+h) % $xfactor , (y+v) % $yfactor]]))
                        then
                            torus[[(x+h) % $xfactor , (y+v) % $yfactor]] = babies[0]
                            babies = babies - [torus[[(x+h) % $xfactor , (y+v) % $yfactor]]]
                        end
                    end
                end
            
            end
        end

        #check unique number of bitstrings
        strings = []
        pop.each do |gene|
            puts "#{gene.geneid}: #{gene.parents}"
            puts gene.fitness
            #puts gene.bitstr
            strings << gene.bitstr
        end
        populationsize << strings.uniq.size
        puts "@@@"

        #mutate
        $xfactor.times do |x|
            $yfactor.times do |y| 
                torus[[x,y]].mutate
            end
        end

    end

    #generate ending population
    pop = []
    $xfactor.times do |x|
       $yfactor.times do |y|
           pop << torus[[x,y]]
        end
    end

    #display information about population
    pop.sort!{ |b,a| a.fitness <=> b.fitness }
    topfive(pop)
    avall(pop,averagehistory)
    showleader(pop,leaderhistory)
    variance(pop,averagehistory,variancehistory)

    #plot graph of fitness
    Gnuplot.open do |gp|
          Gnuplot::Plot.new( gp ) do |plot|
            plot.title  "Fitness over time"
            plot.ylabel "Fitness"
            plot.xlabel "Iteration"
            x = (0..$iterations).collect do |v|
                v.to_f
            end

            y = x.collect do |v|
                leaderhistory[v]
            end
            
            z = x.collect do |v|
                averagehistory[v]
            end
            
            plot.data << Gnuplot::DataSet.new( [x,y] ) do |ds|
              ds.with = "linespoints"
              ds.title = "Leader"
            end
            
            plot.data << Gnuplot::DataSet.new( [x,z] ) do |ds|
              ds.with = "linespoints"
              ds.title = "Average"
            end
          
          end
    end

    #plot graph of variance
    Gnuplot.open do |gp|
          Gnuplot::Plot.new( gp ) do |plot| 
            plot.title  "Variance over time"
            plot.ylabel "Variance"
            plot.xlabel "Iteration"
            
            x = (0..$iterations).collect do |v|
                v.to_f
            end
            
            y = x.collect do |v|
                variancehistory[v]
            end
            
            plot.data << Gnuplot::DataSet.new( [x,y] ) do |ds|
              ds.with = "linespoints"
              ds.title = "Leader"
            end
          
          end
    end
end

