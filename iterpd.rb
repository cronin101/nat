#!/usr/bin/ruby

require "gnuplot"

#variables for genetic algorithm population
$popsize = 80
$iterations = 100
$games = 10

#fitness function prisoners dilemma
def dilemma(genepool, gene)

    #rival genes are the entire genepool apart from the current gene
    others = genepool

    #gene has to compete with every rival
    others.each do |other|

        #both gene's history/percieved history of opponent is reset to value from bitstring
        gene.resethistorys
        other.resethistorys

        #iterations of prisoners dilemma with cumulative scoring
        $games.times do

            #each gene's choice is influenced by own history and percieved history of opponent
            genechoice = gene.history + gene.ohistory
            otherchoice = other.history + other.ohistory
            
            #truth chosen by first gene is remembered by both genes
            if gene.bitstr[Integer("0b"+genechoice)] == "1"
                gene.updatehistory("self","1")
                other.updatehistory("other","1")
                gchoice = "truth"
            
            #lie chosen by first gene is remembered by both genes
            else                
                gene.updatehistory("self","0")
                other.updatehistory("other","0")
                gchoice = "lie"
            end

            #truth chosen by second gene is remembered by both genes
            if other.bitstr[Integer("0b"+genechoice)] == "1"
                other.updatehistory("self","1")
                gene.updatehistory("other","1")
                ochoice = "truth"

            #lie chosen by second gene is remembered by both genes
            else
                ochoice = "lie"
                other.updatehistory("self","0")
                gene.updatehistory("other","0")
            end

            #in case of a compromise, both genes gain 3 fitness
            if (gchoice == "truth" && ochoice == "truth")
                gene.incfitness(3)
                other.incfitness(3)

            #when both genes lie, both gain 1 fitness
            elsif (gchoice == "lie" && ochoice == "lie")
                gene.incfitness(1)
                other.incfitness(1)

            #when one gene betrays the other, the betrayer gains 5 fitness and the victim gets nothing
            elsif (gchoice == "lie" && ochoice == "truth")
                gene.incfitness(5)
                other.incfitness(0)
            elsif (gchoice == "truth" && ochoice == "lie")
                gene.incfitness(0)
                other.incfitness(5)
            end

        end
    end
end

#tests fitness of population
def testpop(pop)

    #fitness is reset prior to testing
    pop.each do |gene|
        gene.resetfitness
    end

    pop.each do |gene|
        dilemma(pop,gene)
    end

    #fitness is normalised <= 1
    pop[0].totalfitness = 0;
    pop.each do |gene|
        normalfit = (gene.fitness / Float($popsize * 2 * $games))/5
        gene.resetfitness
        gene.incfitness(Float(normalfit))
        gene.totalfitness=(gene.totalfitness + normalfit)
    end
end

#sorts population by descending fitness
def sortpop(pop)
    pop.sort! { |b,a| a.fitness <=> b.fitness }
end

#shows average fitness of top 5 genes
def topfive(pop)
    top = pop[0..4]
    total = 0
    top.each do |gene|
        total+= gene.fitness
    end
    average = Integer(total/top.size)
    puts "Average fitness of top 5 = #{average}"
    average
end

#shows the gene with the highest fitness
def showleader(pop,fithis)
    leader = pop[0]
    puts "Leader fitness = #{leader.fitness}"
    puts leader.bitstr
    fithis << leader.fitness
end

#show average fitness of all genes
def avall(pop,fithis)
    total = 0
    pop.each do |gene|
        total+= gene.fitness
    end
    average = Float(total/pop.size)
    puts "Average fitness = #{average}"
    average
    fithis << average
end

def variance(pop,mean,varhist)
    total = 0
    pop.each do |gene|
        total+= (gene.fitness-mean[mean.size-1])**2
    end
    total = Float(total/pop.size)
    puts "Variance is #{total}"
    varhist << total
end

#Genotype class definition loaded
require_relative "genotype"

#initial population of random genes
pop = []
$popsize.times do
    pop << Genotype.new("random")
end

#empty arrays of fitness history
leaderhistory = []
averagehistory = []
variancehistory = []
#show starting population info
testpop(pop)
sortpop(pop)
topfive(pop)
avall(pop,averagehistory)
showleader(pop,leaderhistory)

#mutation rate set
pop[0].setrate(100)

#iterations of genetic algorithm
$iterations.times do |time|
    
    #output current generation
    puts "Generation #{time+1}"

    #test population
    testpop(pop)

    #rank population by fitness function
    sortpop(pop)

    #show population info
    topfive(pop)
    avall(pop,averagehistory)
    showleader(pop,leaderhistory)
    variance(pop,averagehistory,variancehistory)

    #puts genes on a roulette wheel for stochastic sampling
    roulettewheel = []
    pop.each do |gene|
        roulettewheel << gene
    end

    #kill weakest 75% of the existing population
    top_fitness = pop[0].fitness
    pop = pop[0..$popsize/4-1]

    #spin the roulette wheel to revive population until a total of half original size
    roulettewheel = roulettewheel - pop
    while pop.size < $popsize/2
        roulettewheel.each do |slot|
            if slot.fitness >= rand()
                roulettewheel = roulettewheel - [slot]
                pop << slot
                break
            end
        end
    end

    #crossover survivors to regenerate the rest of the population
    survivors = [] + pop
    survivors.each do |survivor|
        partner = rand(survivors.size)
        pop << Genotype.new(cross(survivor,pop[partner]))
    end

    #mutate
    pop.each do |gene|
        gene.mutate
    end
end

#show ending population
testpop(pop)
sortpop(pop)
topfive(pop)
avall(pop,averagehistory)
showleader(pop,leaderhistory)
variance(pop,averagehistory,variancehistory)

Gnuplot.open do |gp|
      Gnuplot::Plot.new( gp ) do |plot|
 
        plot.title  "Fitness over time"
        plot.ylabel "Fitness"
        plot.xlabel "Iteration"
        x = (0..$iterations).collect do |v|
            v.to_f
        end
        y = x.collect do |v|
            leaderhistory[v]
        end
        
        z = x.collect do |v|
            averagehistory[v]
        end
        
        plot.data << Gnuplot::DataSet.new( [x,y] ) do |ds|
          ds.with = "linespoints"
          ds.title = "Leader"
        end

        plot.data << Gnuplot::DataSet.new( [x,z] ) do |ds|
          ds.with = "linespoints"
          ds.title = "Average"
        end

      end
 
end

Gnuplot.open do |gp|
      Gnuplot::Plot.new( gp ) do |plot|
 
        plot.title  "Variance over time"
        plot.ylabel "Variance"
        plot.xlabel "Iteration"
        x = (0..$iterations).collect do |v|
            v.to_f
        end
        y = x.collect do |v|
            1/variancehistory[v]
        end
        
        
        plot.data << Gnuplot::DataSet.new( [x,y] ) do |ds|
          ds.with = "linespoints"
          ds.title = "Leader"
        end
      end
 
end

