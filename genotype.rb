#!/usr/bin/ruby

class Genotype
    
    #read only access to: Bitstring, History of gene, Unique ID, Opponent History, Fitness of Gene
    attr_reader :bitstr, :history, :geneid, :ohistory, :fitness
    
    #Number of Genes generated (Used for giving an ID to each new gene)
    @@numgenes = 0

    #reciprocal of mutation rate
    @@mutrate = 500

    #length of the bitstring for each gene
    @@genelength = 70

    #gene constructor with either a given string or randomised gene
    def initialize( basegene )
        @@numgenes+= 1
        if basegene == "random"
            @bitstr = ""
            #set to correct length before randomised
            @@genelength.times do
                @bitstr = @bitstr + "0"
            end
            self.randomise
        elsif
            #given string is used
            self.gene=(basegene.dup)
        end
        @geneid = @@numgenes
        @parents = "none"
    end

    #parents of a gene are recorded
    def recordparents(father,mother)
        @parents = [father,mother]
    end

    #parents of a gene are returned
    def parents
        @parents
    end

    #resets gene fitness for a generation
    def resetfitness
        @fitness = 0
    end

    #fitness increased by value given by trial
    def incfitness(inc)
        @fitness+= inc.to_f
    end

    #resets the total number of games played in a generation
    def resetgames
        @games = 0
    end
    
    #increments the total number of games played in a generation
    def game
        @games += 1
    end

    #returns the total number of games played in a generation
    def games
        @games
    end

    #set mutation rate explicitly
    def setrate(newrate)
        @@mutrate = newrate
    end

    #returns length of gene string (Replacting default .length method)
    def length
        @@genelength
    end

    #holds the total fitness of a generation
    def totalfitness= (total)
        @@totalfitness = total
    end

    #returns the total fitness of a generation
    def totalfitness
        @@totalfitness
    end

    #gene value is a string that is no longer than the maximum and padded if less than
    def gene=(basegene)
        thisgene=basegene.dup
        if thisgene.length > @@genelength
            thisgene = thisgene[0..@@genelength-1]
        end
        while thisgene.length < @@genelength-1
            thisgene = thisgene + "0"
        end
        @bitstr = thisgene
    end

    #replaces a gene with a random gene of the same length
    def randomise
        @bitstr.length.times do |char|
            @bitstr[char] = rand(2).to_s
        end
    end
   
    #resets historys based on last 6 bits of bitstring
    def resethistorys
        sethistory("other",self.bitstr[@@genelength-3..@@genelength])
        sethistory("self",self.bitstr[@@genelength-6..@@genelength-3])
    end
    
    #target history is set explicitly
    def sethistory( target, value )
        if target == "self"
            @history = value[0]+value[1]+value[2]
        elsif target =="other"
            @ohistory = value[0]+value[1]+value[2]
        end
    end

    #target history is updated after a move is performed
    def updatehistory( target, move )
        if target == "self"
            @history = @history[1..3] + move
        elsif target =="other"
            @ohistory = @ohistory[1..3] + move
        end
    end
    
    #causes a gene to be possibly mutated in-place, returning boolean status
    def mutate
        mutated = false
        @bitstr.length.times do |char|
            if rand(@@mutrate) == @@mutrate-1
                mutated = true
                @bitstr[char] = (1-@bitstr[char].to_i).to_s
            end
        end
        mutated
    end

end

#takes two genes and mutates, returns resulting string
def cross(gene1, gene2)
    point = rand(gene1.length-1)+1
    firsth=gene1.bitstr[0..point]
    secondh=gene2.bitstr[(point+1)..gene2.length-1]
    newgene = firsth + secondh
    newgene
end

